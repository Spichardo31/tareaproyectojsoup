
package paquete;

//importando librerias
import java.io.IOException;
import org.jsoup.Jsoup;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@Controller
@EnableAutoConfiguration
public class PrincipalMain {

    
    //Primer Mapeo
    @RequestMapping("/")
    @ResponseBody
    String Probando() {
        //Texto que me retorna el URL
        return "<p><b>Contenido del primer mapeo</b></p>";
    }
    
    //Segundo Mapeo
    @RequestMapping("/jsoup")
    @ResponseBody
    //Nombre de Variable que contiene el URL del primer mapeo        
    String Probando2 () throws IOException {
        //variable (NombreVariable) que contiene el url del primer mapeo
        String NombreVariable = Jsoup.connect("http://localhost:8080/").get().toString();
           //Texto que me retorna el URL del segundo mapeo + el texto del contenido del primero mapeo
        return "<h2>Jsoup</h2>" +NombreVariable;
    }

    public static void main(String[] args) throws Exception {
        //Corriendo Spring Boot
        SpringApplication.run(PrincipalMain.class, args);
    }
}